const path = require('path')

module.exports = {
    i18n: {
      defaultLocale: "ar",
      locales: ["ar", "en","fr"],
    },
    localePath: path.resolve('./public/locales/')
  };