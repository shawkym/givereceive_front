
module.exports ={
  
  reactStrictMode: true,
  images: {
    unoptimized: true,
  },
  env: {
    API_URL: process.env.API_URL,
    SECRET: process.env.SECRET
  },
};
