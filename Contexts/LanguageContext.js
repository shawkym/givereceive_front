import { createContext, useEffect, useState } from "react";
import i18n from "i18next";
import { useTranslation } from "react-i18next";

export const LanguageContext = createContext();

export default function LanguageProvider({ children }) {

  const [selectedLanguage, setSelectedLanguage] = useState(i18n.language);
  const [styleDir, setStyleDir] = useState({direction:"rtl"});

  useEffect(() => {
    if(selectedLanguage == "ar")
      setStyleDir({direction: "rtl"})
    else
      setStyleDir({direction: "ltr"})
  },[selectedLanguage])
  
  

  const handleLanguageChange = (event) => {
    const languageCode = event.target.value;
    i18n.changeLanguage(languageCode);
    setSelectedLanguage(languageCode);
  };

  const getLanguageName = (language) => {
    switch (language) {
      case "ar":
        return "Arabic - العربية";
      case "en":
        return "English - الانكليزية";
      case "tr":
        return "Türkçe - Turkish";
      case "fr":
          return "French - Français";
    }
  };


  return (
    <LanguageContext.Provider
      value={{
        selectedLanguage,
        handleLanguageChange,
        getLanguageName,
        styleDir
      }}
    >
      {children}
    </LanguageContext.Provider>
  );
}

