
// In order to expose a variable to the browser you have to prefix the variable with NEXT_PUBLIC_. :
const otp = require('simpleotp');
const totp = new otp.Totp();
import axios from 'axios';

const API = axios.create({
        baseURL: process.env.API_URL
});

API.defaults.headers.common['Authorization'] = totp.createToken({secret:process.env.SECRET,seconds :Date.now()/1000});

setInterval(function(){
        API.defaults.headers.common['Authorization'] = totp.createToken({secret:process.env.SECRET,seconds :Date.now()/1000});
      }, 1000);

export default API;


