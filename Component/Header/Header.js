import Link from "next/link";
import { useContext } from "react";
import { useTranslation } from "react-i18next";
import { LanguageContext } from "../../Contexts/LanguageContext";

import styles from "./Header.module.css";

const Header = () => {
  const { t } = useTranslation();
  const { styleDir } = useContext(LanguageContext);

  return (
    <div style={styleDir} className={styles.header}>
      <h1 className={styles.title}>{t("HEADER_TITLE")}</h1>
      <p className={styles.description}>{t("DONOR_DESCRIPTION")}</p>
      <p className={styles.description}>{t("RECIPIENT_DESCRIPTION")}</p>
      <div style={{ display: "flex" }}>
        <Link href="/page/give">
          <button className={"btn " + styles.btn}>{t("GIVE_BUTTON")}</button>
        </Link>
        <Link href="/page/receive">
          <button className={"btn " + styles.btn}>{t("RECEIVE_BUTTON")}</button>
        </Link>
      </div>
    </div>
  );
};

export default Header;
