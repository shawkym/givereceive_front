import styles from "./Slogan.module.css";
import LanguageSwitcher from "../LanguageSwitcher/Switcher";
import Image from "next/image";

function Slogan({}) {
  return (
    <div className={styles.root}>
      <div className={styles.logoWrapper}>
        <Image className={styles.logo} src="/logo.png" width={72} height={72} />
      </div>

      <div>
        <LanguageSwitcher />
      </div>
    </div>
  );
}

export default Slogan;
