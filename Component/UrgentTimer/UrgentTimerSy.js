import React, { useState, useEffect } from 'react';
import styles from "./UrgentTimers.module.css";

const UrgentTimer = () => {
  const [time, setTime] = useState(new Date());

  useEffect(() => {
    const intervalId = setInterval(() => {
      setTime(new Date());
    }, 1000);
    return () => clearInterval(intervalId);
  }, []);

  const startDate = new Date('2023-02-06T01:17:00.000Z');
  const diff = time - startDate;
  const days = Math.floor(diff / (1000 * 60 * 60 * 24));
  const hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  const minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = Math.floor((diff % (1000 * 60)) / 1000);

  return (
    <div>
       <p className={styles.tablertl}>
       مضى على الزلزال بتوقيت سوريا {days} أيام, {hours} ساعة, {minutes} دقيقة, {seconds} ثواني 
         شباط 6, 04:17 UTC+3. 
      </p>
    </div>
  );
};

export default UrgentTimer;