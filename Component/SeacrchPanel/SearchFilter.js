import React, { useContext, useState } from "react";
import Popup from "../Popup/Popup";
import Spinner from "../Spinner/Spinner";
import styles from "./SearchPanel.module.css";

import { useTranslation } from "react-i18next";
import { FaArrowDown } from "react-icons/fa";
import { LanguageContext } from "../../Contexts/LanguageContext";

export default function SearchFilter({
  allLabel,
  dataArraySelected,
  btnChooseLabel,
  allData,
  addToSearchFun,
  pluralOfFilter,
  bgdefault,
}) {
  
  const [showPopup, setShowPopup] = useState(false);
  const { t } = useTranslation();
  const {selectedLanguage} = useContext(LanguageContext);

  return (
    <>
      <div className={styles.searchFilter}>
        {/* <h4>{searchByLabel}: </h4> */}

        {/* <div className={styles.citiesChosen}>
          {dataArraySelected.length == 0 ? (
            <p> {allLabel}</p>
          ) : (
            dataArraySelected.map((ele, idx) => (
              <p style={{ fontWeight: 800 }}>
                {ele}
                {idx == dataArraySelected.length - 1 ? "" : ","}
              </p>
            ))
          )}
          
        </div> */}

        <button
          onClick={() => setShowPopup(true)}
          className={`btn ${styles.searchFilterBtn} ${
            dataArraySelected.length > 0 ? styles.active : ""
          }`}
          style={
            bgdefault && dataArraySelected.length == 0
              ? { background: `${bgdefault}` }
              : {}
          }
        >
          {btnChooseLabel}
          {" ( "}{" "}
          {!dataArraySelected || dataArraySelected.length == 0
            ? allLabel
            : dataArraySelected.length + " " + pluralOfFilter}
          {" ) "}
          <FaArrowDown />
        </button>
      </div>

      <Popup title={btnChooseLabel} show={showPopup} setShow={setShowPopup}>
        <div className={styles.citiesDiv}>
          {/* If there are cities, map through them and create a div for each */}
          {allData && allData.length > 0 ? (
            allData.map((ele, idx) => (
              <div
                // Call addCitToSrch function on click
                onClick={() => addToSearchFun(ele)}
                // Add 'active' class to selected cities
                className={
                  dataArraySelected.includes(
                    typeof ele == "object" ? ele.tag : ele
                  )
                    ? styles.active
                    : ""
                }
                key={idx}
              >
                {/* Display the name of the city */}
                <p>
                  {typeof ele == "object"
                    ? selectedLanguage == "ar"
                      ? ele.tag
                      : selectedLanguage == "fr" ? ele.tag_fr : ele.tag_eng
                    : ''}
                </p>
                <p>
                  {typeof ele == "object"
                    ? selectedLanguage == "ar"
                      ? ele.city_name_ar
                      : ele.city_name
                    : ''}
                </p>
              </div>
            ))
          ) : (
            <Spinner />
          )}
        </div>
        <div className={styles.popupFoot}>
        </div>
      </Popup>
    </>
  );
}
