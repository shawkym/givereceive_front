import React, { useState, useEffect } from "react";
import { useRouter } from "next/router";
import API  from "../../apiConfig";
import styles from "./SearchPanel.module.css";
import { useTranslation } from "react-i18next";
import SearchFilter from "./SearchFilter";

export default function SearchPanel({
  displayedCards,
  setDisplayedCards,
  cardsType,
  setIsFiltering,
}) {
  const { t } = useTranslation();
  const [citiesSelected, setCitiesSelected] = useState([]);
  const [tagsSelected, setTagsSelected] = useState([]);
  const [tags, setTags] = useState([]);
  const [cities, setCities] = useState([]);
  const [qry, setQry] = useState("");
  const [citiesNotAdded, setCitiesNotAdded] = useState(true);
  const [tagsNotAdded, setTagsNotAdded] = useState(true);
  const [fetchingData, setFetchingData] = useState(false);

  const router = useRouter();
  const { query } = router;

  useEffect(() => {
    if (
      tags.length > 0 &&
      query.tag &&
      tags.find((ele) => ele.id == query.tag)
    ) {
      addTagToSrch(tags.find((ele) => ele.id == query.tag));
    }
  }, [query, tags]);

  // function to add a city to the selected cities list
  const addCitToSrch = (ele) => {
    setCitiesSelected((prev) => {
      if (prev.includes(ele)) {
        // city already exists, remove it from the array
        return prev.filter((e) => e !== ele);
      } else {
        // city doesn't exist, add it to the array
        return [...prev, ele];
      }
    });
  };

  // function to add a tag to the selected tags list
  const addTagToSrch = (ele) => {
    setTagsSelected((prev) => {
      if (prev.includes(ele.tag)) {
        // tag already exists, remove it from the array
        return prev.filter((e) => e !== ele.tag);
      } else {
        // tag doesn't exist, add it to the array
        return [...prev, ele.tag];
      }
    });
  };

  // effect that runs whenever the user changes the selected cities, tags, or query string
  useEffect(() => {
    // update the filtering state
    if (citiesSelected.length === 0 && tagsSelected.length === 0 && !qry) {
      setIsFiltering([]);
    } else {
      // fetch cards based on the selected filters
      setFetchingData(true);
      const filters = {
        city: citiesSelected.length > 0 ? JSON.stringify(citiesSelected) : null,
        tags: tagsSelected.length > 0 ? JSON.stringify(tagsSelected) : null,
        title: qry ? (/^\d+$/.test(qry) ? qry : null) : null,
        phoneNumber: qry ? (/^\d+$/.test(qry) ? qry : null) : null,
        details: qry ? (/^\d+$/.test(qry) ? qry : null) : null,
      };
      API
        .get(cardsType + "/filter", { params: filters })
        .then((res) => {
          setFetchingData(false);
          setDisplayedCards([...res.data]);
          setIsFiltering(
            new Array(
              res.data.count,
              citiesSelected.length,
              tagsSelected.length,
              qry
            )
          );
        })
        .catch((ex) => console.error(ex));
      // update the displayed cards state with the filtered results
    }
    // fetch the cities if they haven't been added yet
    if (citiesNotAdded) {
      API
        .get("City/getAll")
        .then((res) => {
          const citys = res.data;
          setCities(Array.from(new Set(citys)));
          setCitiesNotAdded(false);
        })
        .catch((ex) => console.error(ex));
    }

    // fetch the tags if they haven't been added yet
    if (tagsNotAdded) {
      API
        .get( "Tags/getAll")
        .then((res) => {
          const filteredTags = res.data.filter((tag) =>
            tag.type.toLowerCase().includes(cardsType)
          );
          setTags(Array.from(new Set(filteredTags)));
          setTagsNotAdded(false);
        })
        .catch((ex) => console.error(ex));
    }
  }, [citiesSelected, tagsSelected, qry]);
  useEffect(() => {
    if (
      localStorage.getItem("SelectedCitiesFromLanding") &&
      localStorage.getItem("SelectedTagsFromLanding")
    ) {
      const citiesSelect = JSON.parse(
        localStorage.getItem("SelectedCitiesFromLanding")
      );
      const tagsSelect = JSON.parse(
        localStorage.getItem("SelectedTagsFromLanding")
      );

      if (citiesSelect != null) {
        setCitiesSelected(citiesSelect);
      }
      if (tagsSelect != null) {
        setTagsSelected(tagsSelect);
      }

      localStorage.setItem("SelectedCitiesFromLanding", null);
      localStorage.setItem("SelectedTagsFromLanding", null);
    }
  }, []);

  return (
    // The main container for the search panel
    <div className={styles.SearchPanel}>
      <h2 style={{marginBottom: 2}}> {t("SEARCH")}</h2>

      {/* The container for the search input */}
      <div>
        {fetchingData ? (
          <h5>{t("SEARCHING")}</h5>
        ) : (
          ""
          // <h5>{t("SEARCH_BY_WORD_OR_PHONE")}:</h5>
        )}

        <div className={`${styles.form__group} ${styles.field}`}>
        <h5>{t("SEARCH_IN_ALL_PAGES")}</h5>
          <input
            // Input type is text
            type="text"
            // Value of the input is set to the value of 'qry'
            value={qry}
            // Call setQry function on input change
            onChange={(e) => setQry(e.target.value)}
            // Add CSS class to the input element
            className={styles.form__field}
            // Placeholder text for the input
            placeholder='->'
            // Input is required
            required
          />
        </div>
      </div>

      {/* The container for the city filter */}
      <div className={styles.flex}>
        <SearchFilter
          allLabel={t("ALL_CITIES")}
          btnChooseLabel={t("CITY_CHOOSE")}
          pluralOfFilter={t("CITIES")}
          dataArraySelected={citiesSelected}
          allData={cities}
          addToSearchFun={addCitToSrch}
        />
        <SearchFilter
          allLabel={t("ALL_TAGS")}
          btnChooseLabel={t("TAG_CHOOSE")}
          pluralOfFilter={t("TAGS")}
          dataArraySelected={tagsSelected}
          allData={tags}
          addToSearchFun={addTagToSrch}
        />
      </div>
    </div>
  );
}
