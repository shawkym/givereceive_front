import API  from "../../apiConfig";

export function isStringOnlyZeros(str) {
  // Remove any leading or trailing whitespace from the string

  str = str.trim();

  // Check if the string contains only zeros
  return str === '0'.repeat(str.length);
}

export function checkCardsUserProperty (id, type) {
  const prevData = localStorage.getItem(type)
    ? JSON.parse(localStorage.getItem(type))
    : new Array();
  return prevData.find((pp) => pp == id)
}

export const checkCalledCards = (data) => {
  const alreadyCalledData = localStorage.getItem("alreadycalledlist")
    ? JSON.parse(localStorage.getItem("alreadycalledlist"))
    : new Array();
  Array.isArray(alreadyCalledData)
    ? alreadyCalledData.forEach((e) => {
      data.forEach((ele) => {
        if (ele._id == e) ele.isFeedbackable = true;
      });
    })
    : {};
}
export function checkFeedbackedCards(data) {
  const alreadyFeedbackedData = localStorage.getItem("feedbackedlist")
    ? JSON.parse(localStorage.getItem("feedbackedlist"))
    : new Array();
  Array.isArray(alreadyFeedbackedData)
    ? alreadyFeedbackedData.forEach((e) => {
      data.forEach((ele) => {
        if (ele._id == e) ele.isFeedbackable = false;
      });
    })
    : {}
  return data;
}

// A function for counting the number of local items of a given type
export const countLocalOfType = (data, type) => {
  const localofType = localStorage.getItem(type)
    ? JSON.parse(localStorage.getItem(type))
    : new Array();
  return data.filter((ele) => {
    localofType.find(ele._id)
  }).length;
};

// A function for marking an item as called (by adding its ID to local storage)
export const markCalled = async (cardData, setData) => {
  // Get the list of already-called items from local storage
  const alreadyCalled = localStorage.getItem("alreadycalledlist")
    ? JSON.parse(localStorage.getItem("alreadycalledlist"))
    : new Array();
  // Add the ID of the newly-called item to the list
  alreadyCalled.push(cardData._id);
  cardData.isFeedbackable = true;
  setData(new Array(cardData));
  // Store the updated list back in local storage
  localStorage.setItem("alreadycalledlist", JSON.stringify(alreadyCalled));
};

// A function for deleting an item from the database and local storage
export const deleteItem = async (type, id) => {
  // Send a request to the server to delete the item with the given ID
  const { data } = await API.post( `${type}/delete`, { _id: id });
  // Get the list of IDs for the given type from local storage
  const prev = localStorage.getItem(`${type}`)
    ? JSON.parse(localStorage.getItem(type))
    : new Array();
  // Filter out the ID of the deleted item and store the updated list back in local storage
  const newStrg = prev.filter((el) => el != id);
  localStorage.setItem(`${type}`, JSON.stringify(newStrg));
};

// A function for mark an item from the database and local storage as fulfilled
export const fulfillItem = async (type, id, setShouldHide) => {
  // Send a request to the server with the given ID
  const { data } = await API.post( `${type}/fulfill`, { _id: id });
  // Get the list of IDs for the given type from local storage
  const prev = localStorage.getItem(`${type}`)
    ? JSON.parse(localStorage.getItem(type))
    : new Array();
  // Filter out the ID of the deleted item and store the updated list back in local storage
  const newStrg = prev.filter((el) => el != id);
  localStorage.setItem(`${type}`, JSON.stringify(newStrg));
  setShouldHide(true);
};

// A function for adding feedback to an item
export const addFeedback = async (type, id, setShouldRefresh) => {
  // Send a request to the server to add feedback to the item with the given ID
  const { data } = await API.post( `${type}/feedback`, { _id: id });
  // Get the list of already-feedbacked items from local storage
  const alreadyFeedbacked = localStorage.getItem("feedbackedlist")
    ? JSON.parse(localStorage.getItem("feedbackedlist"))
    : new Array();
  // If the feedback was added successfully, add the ID of the item to the list and store it back in local storage
  if (data) {
    alreadyFeedbacked.push(id);
    localStorage.setItem("feedbackedlist", JSON.stringify(alreadyFeedbacked));
    setShouldRefresh(true);
  }
};

// A function for adding a flag to an item
export const addFlag = async (type, id, setShouldRefresh) => {
  // Send a request to the server to add a flag to the item with the given ID
  const { data } = await API.post( `${type}/flag`, { _id: id });
  // Get the list of already-feedbacked items from local storage (this is probably a mistake - should be "alreadyFlagged")
  const alreadyFeedbacked = localStorage.getItem("feedbackedlist")
    ? JSON.parse(localStorage.getItem("feedbackedlist"))
    : new Array();
  // If the flag was added successfully, add the ID of the item to the list and store it back in local storage
  if (data) {
    alreadyFeedbacked.push(id);
    localStorage.setItem("feedbackedlist", JSON.stringify(alreadyFeedbacked));
    setShouldRefresh(true);
  }
};



