import { countLocalOfType } from "./CardsHelper";
import { useTranslation } from "react-i18next";

export default function NoticeHelper({ pageType }) {
  const { t } = useTranslation();

  switch (pageType) {
    case "give":
      const giveCount = countLocalOfType(pageType);
      return giveCount > 0 ? (
        <p className="notice">
          {t("YOU_HAVE_OFFERS_NUMBER")} {giveCount} <br></br> {t("PLEASE_REMOVE_IF_OFFERED")}
        </p>
      ) : (
        ""
      );
    case "receive":
      return null; // or a component for receive type
    default:
      return null; // or a default component
  }
}