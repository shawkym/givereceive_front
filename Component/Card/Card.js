import React, { useEffect, useState } from "react";
import { BiPhoneCall, BiTrash, BiLike, BiDislike, BiMap } from "react-icons/bi";
import styles from "./Cards.module.css";
import moment from "moment";
import { API_URL } from "../../apiConfig";

import "moment/locale/ar";
import "moment/locale/fr";
import "moment/locale/tr";

import { arabicMonths } from "./arabicMonths";
import {
  addFlag,
  addFeedback,
  markCalled,
  fulfillItem,
  deleteItem,
  isStringOnlyZeros,
  checkCardsUserProperty,
} from "./CardsHelper";
import { useTranslation } from "react-i18next";
import i18n from "i18next";

export default function Card({ cardData, cardType }) {
  const [data, setData] = useState(cardData);
  const [tags, setTags] = useState([]);
  const { t } = useTranslation();
  const [shouldHide, setShouldHide] = useState(false)
  const [shouldRefresh, setShouldRefresh] = useState(false)

  // Update moment.js locale with Arabic month names
  useEffect(() => {
    moment.updateLocale("ar", { months: arabicMonths });
  }, [cardType, data, shouldHide,shouldRefresh]);

  // Render the card with its elements and buttons
  return (
    <div key={cardData._id} hidden={shouldHide ? "" : "hidden"} className={shouldHide ? "" : styles.card}>
      {/* Card header with title, if available */}
      <div className={styles.title}>
        {cardData.title ? <h3>{cardData.title} </h3> : ""}
        <hr className={styles.hr}></hr>
      </div>
      {/* Feedback and flag buttons */}
      <div className={styles.divfeedback}>
        {
          // Like button, only shown if the card is feedbackable
          <button
            className={
              cardData.isFeedbackable
                ? styles.feedbackbtn
                : styles.feedbackcounter
            }
            onClick={
              cardData.isFeedbackable
                ? () => addFeedback(cardType, cardData._id, setShouldRefresh)
                : () => {}
            }
          >
            <div className={styles.iconsContainer}>
              <BiLike className={styles.icons} />
              <div>{cardData.feedbacked}</div>
            </div>
          </button>
        }
        {
          // Dislike button, only shown if the card is feedbackable
          <button
            className={
              cardData.isFeedbackable ? styles.flagbtn : styles.flagcounter
            }
            onClick={
              cardData.isFeedbackable
                ? () => addFlag(cardType, cardData._id, setShouldRefresh)
                : () => {}
            }
          >
            <div className={styles.iconsContainer}>
              <BiDislike className={styles.icons} />
              <div>{cardData.flagged}</div>
            </div>
          </button>
        }
      </div>
      {/* Tags, if available */}

      {cardData.tags ? (
        <div className={styles.tags}>
          {cardData.tags.map((ee, idx) =>
            ee.trim() != "" ? (
              <div key={idx} className={styles.active}>
                <a>
                  {ee}
                </a>
              </div>
            ) : (
              ""
            )
          )}
        </div>
      ) : (
        ""
      )}

      {/* City, if available */}
      {cardData.city ? (
        <h5 className={styles.city}>
          {" "}
          <BiMap className={styles.icons} /> {cardData.city}
        </h5>
      ) : (
        ""
      )}

      <div className={styles.cardDetails}>
        {/* Details, if available */}
        {cardData.details ? <p> {cardData.details}</p> : ""}

        {/* Creation date, if available */}
        {cardData.createdAt ? (
          <p className={styles.cardDate}>
            {" "}
            {t("PUBLISHED_AT")}{" "}
            {moment(cardData.createdAt)
              .locale(i18n.language)
              .format(t("DATE_FORMAT"))}{" "}
          </p>
        ) : (
          ""
        )}

        {/* Phone number, if available */}
        {cardData.phoneNumber && !isStringOnlyZeros(cardData.phoneNumber) ? (
          <p> {cardData.phoneNumber}</p>
        ) : (
          ""
        )}
      </div>

      {/* Call button */}
      <button
        className={styles.call}
        onClick={() => {
          location.href = `tel:${cardData.phoneNumber}`;
          markCalled(data, setData);
        }}
      >
        {t("CALL")} <BiPhoneCall />
      </button>

      {/*//The checkCardsUserProperty returns true if the id of the item exists in the local storage */}

      {checkCardsUserProperty(cardData._id, cardType) ? (
        <button
          className={styles.deletebtn}
          onClick={() => fulfillItem(cardType, cardData._id, setShouldHide)}
        >
          {t("REMOVE")}
          <BiTrash />
        </button>
      ) : (
        ""
      )}
    </div>
  );
}


