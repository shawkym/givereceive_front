import Link from "next/link";
import React, { useState, useEffect, useContext } from "react";
import { useTranslation } from "react-i18next";
import styles from "./HomeLanding.module.css";
import SearchFilter from "../SeacrchPanel/SearchFilter";
import { useRouter } from "next/router";
import { LanguageContext } from "../../Contexts/LanguageContext";

export default function HomeLanding() {
  const { t } = useTranslation();
  const router = useRouter();
  const { styleDir } = useContext(LanguageContext);
 
  useEffect(() => {
  
  }, []);

  return (
    <div className={styles.root}>
      <div className={styles.header} style={styleDir}>
        <h1 className={styles.title}>{t("HEADER_TITLE")}</h1>
        <p className={styles.description}>{t("DONOR_DESCRIPTION")}</p>
        <p className={styles.description}>{t("RECIPIENT_DESCRIPTION")}</p>
        <div style={{ display: "flex" }}>
          <Link href="/page/give">
            <button className={styles.btn}>{t("GIVE_BUTTON")}</button>
          </Link>
          <Link href="/page/receive">
            <button className={styles.btn}>{t("RECEIVE_BUTTON")}</button>
          </Link>
        </div>
        <p className={styles.dangerous}>{t("WARNING_SERVICES")}</p>
      </div>
    </div>
  );
}
