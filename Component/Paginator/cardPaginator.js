import React, { useEffect, useState } from "react";
import Card from "../Card/Card";
import styles from "./cardPaginator.module.css";
import API  from "../../apiConfig";

export default function CardPaginator({
  displayedCards,
  setDisplayedCards,
  itemsType,
  setIsLoading,
  isFiltering,
}) {
  // State for the current page number
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [currentItems, setCurrentItems] = useState([]);
  const [cardType, setCardType] = useState(itemsType);

  // Get the currentCards for the current page based on the current page number and the number of currentCards per page

  // Calculate the total number of pages based on the total number of currentCards and the number of currentCards per page
  // Watch for changes in the router.pathname value
  // Handler for clicking a page number button
  const handleClick = (pageNumber) => setCurrentPage(pageNumber);

  // Define an asynchronous function called fetchData
  async function fetchData() {
    // Fetch the data using the API URL, the itemsType (give or receive), and the currentPage number
    // Update the currentItems and totalPages states with the result from the API or passed cards if any
    setIsLoading(true);
    if (displayedCards?.count > 0) setCurrentItems(displayedCards);
    else {
      const result = await API.get(
       itemsType + "/offset/" + currentPage
      );
      setCurrentItems([...result.data.resultArray]);
      setTotalPages(result.data.numberOfPages);
      setDisplayedCards(result.data.resultArray);
    }
  }

  // Run the useEffect hook every time the currentPage changes
  useEffect(() => {
    // Call the fetchData function
    if (isFiltering.length === 0) {
      fetchData();
    } else {
      setTotalPages(Math.ceil(displayedCards.length / 10));
      setCurrentItems(
        displayedCards.slice(currentPage * 10 - 10, currentPage * 10)
      );
    }
    setIsLoading(false);
    // Re-run the useEffect hook every time the currentPage value changes
  }, [currentPage, cardType, isFiltering]);
  const PaginatorDiv = () =>
    totalPages > 1 && (
      <div className={styles.paginator}>
        {/* Render the previous page button if not on the first page */}
        {currentPage > 1 && (
          <button
            className={`${styles.paginatorbutton} ${styles.prev}`}
            onClick={() => setCurrentPage(currentPage - 1)}
          >
            &lt;
          </button>
        )}

        {/* Render a button for each page */}
        {Array.from(Array(totalPages).keys()).map((pageNumber) => (
          <button
            className={`${styles.paginatorbutton} ${
              currentPage === pageNumber + 1 ? styles.currentpage : ""
            }`}
            key={pageNumber}
            onClick={() => handleClick(pageNumber + 1)}
          >
            {pageNumber + 1} / {totalPages}
          </button>
        ))}

        {/* Render the next page button if not on the last page */}
        {currentPage < totalPages && (
          <button
            className={`${styles.paginatorbutton} ${styles.nxt}`}
            onClick={() => setCurrentPage(currentPage + 1)}
          >
            &gt;
          </button>
        )}
      </div>
    );

  return (
    <div>
      <PaginatorDiv />
      {/* Render the cards for the current page */}
      <div className={styles.cardsContainer}>
        {currentItems.map((item) => (
          <div key={item._id} className={styles.cardWrapper}>
            <Card key={item._id} cardData={item} cardType={itemsType} />
          </div>
        ))}
      </div>
      <PaginatorDiv />
    </div>
  );
}

// get Page Type from URL
export async function getStaticPaths() {
  const paths = [
    { params: { cardsPage: "give" } },
    { params: { cardsPage: "receive" } },
    { params: { cardsPage: "lost" } },
    { params: { cardsPage: "found" } },
    { params: { cardsPage: "reclaim" } },
  ];
  return { paths, fallback: "blocking" };
}

// pass page type to component
export async function getStaticProps({ params }) {
  const itemsType = params.cardsPage;
  await fetchData();
  return {
    props: {
      itemsType,
    },
  };
}
