import { useContext, useState } from "react";
import i18n from "i18next";
import styles from "./Switcher.module.css";
import { LanguageContext } from "../../Contexts/LanguageContext";

const LanguageSwitcher = ({ footer }) => {
  const { selectedLanguage, handleLanguageChange, getLanguageName } =
    useContext(LanguageContext);

  return (
    <select
      className={`${styles.select} ${footer ? styles.footer : ""}`}
      value={selectedLanguage}
      onChange={handleLanguageChange}
    >
      {Object.keys(i18n.services.resourceStore.data).map((language) => (
        <option key={language} value={language}>
          {getLanguageName(language)}
        </option>
      ))}
    </select>
  );
};

export default LanguageSwitcher;
