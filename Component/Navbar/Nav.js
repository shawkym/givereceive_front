// Importing necessary modules and components
import { useState, useEffect, useContext } from "react";
import Link from "next/link";
import { IoMenu, IoCloseSharp } from "react-icons/io5";
import { FaFacebook } from "react-icons/fa";
import { useRouter } from "next/router";
import { key } from "node_env";
import { useTranslation } from "react-i18next";
import Image from "next/image";
import Slogan from "../Slogan/Slogan";
import styles from "./Nav.module.css";
import API  from "../../apiConfig";
import Header from "../Header/Header";
import LanguageSwitcher from "../LanguageSwitcher/Switcher";
import { LanguageContext } from "../../Contexts/LanguageContext";

// NavDesktop component
const NavDesktop = () => {
  // Get the current path using useRouter hook
  const cardPages = ["urgent", "reclaim", "found", "lost", "receive", "give"];
  // Render the desktop navigation bar
  const router = useRouter();
  const currentPath = router.pathname;
  const { t } = useTranslation();
  const [top, setTop] = useState(25);

  useEffect(() => {
    function handleScroll() {
      if (window.scrollY > 25) {
        setTop(0);
      } else {
        setTop(25);
      }
    }

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <nav style={{ top: top }} className={styles.nav + " " + styles.navDesktop}>
      <Slogan />
      <ul>
        {cardPages.map((page, key) => {
          return (
            <li
              key={key}
              className={
                router.query.cardsPage?.includes(page) ? styles.active : page.includes('urgent') ? styles.lidanger : ""
              }
            >
              <a href={`/page/${page}`}>{t(page.toUpperCase())}</a>
            </li>
          );
        })}

        <li className={currentPath === "/" ? styles.active : ""}>
          <Link href={"/"}>{t("HOME")}</Link>
        </li>
      </ul>
    </nav>
  );
};

// NavMobile component
const NavMobile = ({ tags }) => {
  // State to control mobile menu open/close
  const [MobileListOpen, setMobileListOpen] = useState(false);
  const { t } = useTranslation();

  // Get the current path using useRouter hook
  const router = useRouter();
  const currentPath = router.pathname;
  const cardPages = ["urgent","receive", "give", "reclaim", "found", "lost"];

  // Render the mobile navigation bar
  return (
    <nav key={key} className={styles.navMobile}>
      <IoMenu onClick={() => setMobileListOpen(true)} />
      <Slogan mobile />

      <div
        className={styles.mobList + " " + (MobileListOpen ? styles.open : "")}
      >
        <IoCloseSharp
          className={styles.close}
          onClick={() => setMobileListOpen(false)}
        />
        <ul>
          <li>
            <Link href={"/"}>
              <a
                className={currentPath === "/" ? styles.active : ""}
                onClick={() => setMobileListOpen(false)}
              >
                {t("HOME")}
              </a>
            </Link>
          </li>
          {cardPages.map((page, key) => (
            //I know this is not working as next standards specify, however I'm unable to debug this for the time being
            <li
              key={key}
              className={
                router.query.cardsPage?.includes(page) ? styles.active : page.includes('urgent') ? styles.lidanger : ""
              }
            >
             <a href={`/page/${page}`}>{t(page.toUpperCase())}</a>
              {/*  {page === "give" ? (
                <ul className={styles.subMenu}>
                  {tags &&
                    tags
                      .filter((f) => f.showInSub)
                      .map((f, idx) => (
                        <Link key={idx} href={`/page/give?tag=${f.id}`}>
                          <a onClick={() => setMobileListOpen(false)}>
                            {t(f.tag)}
                          </a>
                        </Link>
                      ))}
                </ul>
              ) : (
                ""
              )} */}
            </li>
          ))}
        </ul>
      </div>
    </nav>
  );
};

// Component for displaying the navigation bar
const Nav = () => {
  // State variables for all tags from the API
  const [tags, setTags] = useState([]);
  const [tagsNotAdded, setTagsNotAdded] = useState(true);
  const router = useRouter();
  const { t } = useTranslation();

  // Function for fetching tags from the API when tagsNotAdded is true
  useEffect(() => {
    const func = async () => {
      try {
        const resTags = await API.get("tags/getAll");
        setTags(resTags.data);
        setTagsNotAdded(false);
      } catch (ex) {
        console.error("ex: ", ex);
      }
    };
    if (tagsNotAdded) func();
  }, []);

  // Render the desktop and mobile navigation bars
  return (
    <>
      <NavDesktop />
      <NavMobile tags={tags} />
    </>
  );
};

// Component for displaying the site footer
export function SiteFooter() {
  const { t } = useTranslation();
  const { styleDir } = useContext(LanguageContext);
  return (
    <footer style={styleDir} className={styles.myfooter}>
      <div className={styles.secondCol}>
        <div className={styles.social}>
          <p>{t("FOLLOW_US")}</p>
          <a
            className={styles.myfb}
            href="https://www.facebook.com/grmorocco"
            target="_blank"
            rel="noopener noreferrer"
          >
            <FaFacebook />
          </a>
        </div>
        {/* <div className={styles.onlyMob}>
          <LanguageSwitcher footer />
        </div> */}
        <div className={styles.privacy}>
          {/* <p>{t("YOUR_PRIVACY_MATTERS")}</p> */}
          <Link
            href={"/privacypolicy"}
            className={styles.primary}
            target="_blank"
          >
            <a className={styles.footerLink}>{t("PRIVACY_POLICY")}</a>
          </Link>
        </div>
      </div>
    </footer>
  );
}

export default Nav;
