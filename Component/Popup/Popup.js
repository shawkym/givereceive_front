import React, { useRef, useEffect } from "react";
import { FaWindowClose } from "react-icons/fa";
import styles from "./Popup.module.css";

export default function Popup({ title, show, children, setShow }) {
  return (
    <>
      {show ? (
        <div className={`${styles.popup} ${show ? styles.active : ""}`}>
          <div className={styles["popup-inner"]}>
            <div className={styles.header}>
              <h2> {title}</h2>
              <FaWindowClose onClick={() => setShow(null)} />
            </div>
            <div className={styles.children}>{children}</div>
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
}


