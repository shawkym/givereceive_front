import React, { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import styles from "./AddPanel.module.css";
import API  from "../../apiConfig";
import SearchFilter from "../SeacrchPanel/SearchFilter";
import { LanguageContext } from "../../Contexts/LanguageContext";

export default function AddPanel(cardsType, displayedCards, setDisplayedCards) {
  //The following states control the inputs
  const [title, setTitle] = useState("");
  const [details, setDetails] = useState("");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [city, setCity] = useState("");
  const [tagsSelected, setTagsSelected] = useState([]);
  const [tags, setTags] = useState([]);
  const [tagsNotAdded, setTagsNotAdded] = useState(true);
  const [flagged, setFlagged] = useState(0);
  const [submitted, setSubmitted] = useState(0);
  const [feedbacked, setFeedbacked] = useState(0);
  const [loading, setLoading] = useState(false);
  const [type, setType] = useState(cardsType.cardsType);
  const { t } = useTranslation();
  //This state to control the collapse of the add form
  const [mobileCollapse, setMobileCollapse] = useState(false);

  // Function to send post request with the body to add a document
  const addForm = async () => {
    const body = {
      title,
      details,
      phoneNumber,
      flagged,
      feedbacked,
      city,
      tags: tagsSelected,
    };

    try {
      setLoading(true);
      setSubmitted(true);
      const { data } = await API.post(
         cardsType.cardsType + "/",
        body
      );
      if (data._id) {
        //if the insertion was successfull, add the inserted item's id to the localstorage
        const prevData = localStorage.getItem(cardsType.cardsType)
          ? JSON.parse(localStorage.getItem(cardsType.cardsType))
          : [];
        prevData.push(data._id);
        localStorage.setItem(cardsType.cardsType, JSON.stringify(prevData));
      }
      //setDisplayedCards(new Array[displayedCards.displayedCards,body])
      setLoading(false);
      setCity("");
      setTitle("");
      setDetails("");
      setPhoneNumber("");
      setTagsSelected([]);
      window.location.reload();
    } catch {
      setLoading(false);
    }
  };

  //When a user click on a tag in the "insert" form
  const addTagsSel = (tag) => {
    var newAr = [...tagsSelected];
    const idx = newAr.findIndex((e) => e === tag.tag);
    if (idx >= 0) {
      newAr.splice(idx, 1);
    } else {
      newAr.push(tag.tag);
    }
    setTagsSelected([...newAr]);
  };

  useEffect(() => {
    const func = async () => {
      try {
        const resTags = await API.get("tags/getAll");
        const filteredTags = await resTags.data.filter((tag) =>
          tag.type.toLowerCase().includes(cardsType.cardsType)
        );
        setTags(filteredTags);
        setTagsNotAdded(false);
      } catch (ex) {
        console.error("ex: ", ex);
      }
    };
    if (tagsNotAdded) func();
  }, [tags,city,title,phoneNumber,details]);

  return (
    <div>
      {mobileCollapse ? (
        ""
      ) : (
        <div className={styles.btnMbl}>
          <button className="btn" onClick={() => setMobileCollapse(true)}>
            {type == "give" ? <h1>{t("ADD_PANEL_GIVE")}</h1> : ""}
            {type == "receive" ? <h1>{t("ADD_PANEL_RECEIVE")}</h1> : ""}
            {type == "lost" ? <h1>{t("ADD_PANEL_LOST")}</h1> : ""}
            {type == "found" ? <h1>{t("ADD_PANEL_FOUND")}</h1> : ""}
            {type == "reclaim" ? <h1>{t("ADD_PANEL_RECLAIM")}</h1> : ""}
            {type == "urgent" ? <h1>{t("ADD_PANEL_URGENT")}</h1> : ""}
          </button>
        </div>
      )}

      <div className={styles.root + " " + (mobileCollapse ? styles.show : "")}>
        <div>
          {type == "give" ? <h1>{t("ADD_PANEL_GIVE")}</h1> : ""}
          {type == "receive" ? <h1>{t("ADD_PANEL_RECEIVE")}</h1> : ""}
          {type == "lost" ? <h1>{t("ADD_PANEL_LOST")}</h1> : ""}
          {type == "found" ? <h1>{t("ADD_PANEL_FOUND")}</h1> : ""}
          {type == "reclaim" ? <h1>{t("ADD_PANEL_RECLAIM")}</h1> : ""}
          {type == "urgent" ? <h1>{t("ADD_PANEL_URGENT")}</h1> : ""}

        </div>
        <div>
          <button
            disabled={
              title == "" ||
              phoneNumber == "" ||
              phoneNumber.length < 4 ||
              details == "" ||
              city == "" ||
              loading ||
              submitted
            }
            onClick={addForm}
            className={styles.btn}
            style={{ marginTop: 40 }}
          >
            {loading ? t("ADD_PANEL_ADDING") : t("ADD_PANEL_ADD")}
          </button>
        </div>
        <div className={styles.form__group + " " + styles.field}>
          <input
            type="input"
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            className={styles.form__field}
            placeholder="Name"
            required
          />
          <label className={styles.form__label}>{t("ADD_PANEL_NAME") + ' *'}</label>
        </div>
        <div className={styles.form__group + " " + styles.field}>
          <input
            type="number"
            value={phoneNumber}
            onChange={(e) => {
              setPhoneNumber(e.target.value);
            }}
            className={styles.form__field}
            placeholder="Phone number"
            required
          />
          <label className={styles.form__label}>
            {t("ADD_PANEL_PHONE_NUMBER")+' *'}
          </label>
        </div>

        <div className={styles.form__group + " " + styles.field}>
          <textarea
            type="text"
            value={city}
            onChange={(e) => setCity(e.target.value)}
            className={styles.form__field}
            rows={1}
            placeholder="City"
            required
          />
          <label className={styles.form__label}>{t("ADD_PANEL_CITY")+' *'}</label>
        </div>
        <div style={{ display: "flex",marginBottom:10, marginTop:10 }}>
          <p>{t("ADD_TAGS")}</p>
          <SearchFilter
            allLabel={t("ALL_TAGS")}
            btnChooseLabel={t("TAG_CHOOSE")}
            pluralOfFilter={t("TAGS")}
            dataArraySelected={tagsSelected}
            allData={tags}
            addToSearchFun={addTagsSel}
          />
        </div>

        <div className={styles.form__group + " " + styles.field}>
          <textarea
            type="input"
            value={details}
            onChange={(e) => setDetails(e.target.value)}
            className={styles.form__field}
            placeholder="Details"
            rows={5}
            required
          />
          <label className={styles.form__label}>{t("ADD_PANEL_DETAILS")+ ' *'}</label>
        </div>

        {/* 
        <div className={styles.tags}>
          {tags &&
            tags.map((ele, idx) => (
              <div
                key={idx}
                onClick={() => {
                  addTagsSel(ele.tag);
                }}
                className={
                  tagsSelected.find((e) => e == ele.tag)
                    ? styles.active
                    : styles.tags
                }
              >
                <p>{ele.tag}</p>
              </div>
            ))} 
            
        </div>
*/}
      </div>
    </div>
  );
}
