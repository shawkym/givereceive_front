import React, { useState, useEffect } from "react";
import axios from "axios";
import "moment/locale/ar";
import moment from "moment";

import styles from "./Urgent.module.css";
import API  from "../../apiConfig";
import UrgentTimer from "../UrgentTimer/UrgentTimer";
import UrgentTimerSy from "../UrgentTimer/UrgentTimerSy";

const arabicMonths = [
  "كانون الثاني",
  "شباط",
  "آذار",
  "نيسان",
  "أيار",
  "حزيران",
  "تموز",
  "آب",
  "أيلول",
  "تشرين الأول",
  "تشرين الثاني",
  "كانون الأول",
];

export default function Urgent() {
  const [initialData, setInitialData] = useState([]);
  const [showData, setShowData] = useState([]);
  const [data, setData] = useState(initialData);
  const [searchTerm, setSearchTerm] = useState("");

  useEffect(() => {
    moment.updateLocale("ar", { months: arabicMonths });
    const getData = async () => {
      let response = await API.get("urgent/");
      let data = response.data;
      setInitialData(data);
      setShowData(
        data.filter(
          (item) =>
            item.name.toLowerCase().includes(searchTerm.toLowerCase()) ||
            item.details.toLowerCase().includes(searchTerm.toLowerCase())
        )
      );
    };
    getData();
  }, [searchTerm]);

  function AddData() {
    const [newData, setNewData] = useState({
      id: "",
      name: "",
      phoneNumber: "",
      city: "",
      details: "",
    });

    const handleChange = (event) => {
      setNewData({ ...newData, [event.target.name]: event.target.value });
    };

    let handleSubmit = async (event) => {
      event.preventDefault();
      setData([...data, { id: initialData.length + 1, ...newData }]);

      try {
        await API.post("urgent/", newData);
      } catch (error) {
        console.error(error);
      }

      setNewData({
        id: initialData.length + 1,
        name: "",
        phoneNumber: "",
        city: "",
        details: "",
      });
    };

    return (
      <form onSubmit={handleSubmit} className={styles.form}>
        <input
          className={styles.input}
          type="text"
          name="name"
          placeholder="الإسم"
          value={newData.name}
          onChange={handleChange}
        />
        <input
          className={styles.input}
          id="phoneNumber"
          type="text"
          name="phoneNumber"
          placeholder="هاتف"
          value={newData.phoneNumber}
          onChange={handleChange}
        />
        <input
          className={styles.input}
          type="text"
          name="city"
          placeholder="المدينة"
          value={newData.city}
          onChange={handleChange}
        />
        <input
          className={styles.input}
          type="text"
          name="details"
          placeholder="معلومات إضافية"
          value={newData.details}
          onChange={handleChange}
        />
        <button className={styles.button} type="submit">
          إضافة
        </button>
      </form>
    );
  }

  function DataTable({ data }) {
    return (
      <table className={styles.table}>
        <thead>
          <tr className={styles.tr}>
            <th className={styles.th}>#</th>
            <th className={styles.th}>الإسم</th>
            <th className={styles.th}>المدينة</th>
            <th className={styles.th}>هاتف</th>
            <th className={styles.th}>معلومات إضافية</th>
          </tr>
        </thead>
        <tbody className={styles.tbody}>
          {data.map((item) => (
            <tr key={item.id} className={styles.tr}>
              <td className={styles.td}>
                {moment(item.createdAt)
                  .locale("ar")
                  .format("DD MMMM ، الساعة: h:mm a")}{" "}
              </td>
              <td className={styles.td}>{item.name}</td>
              <td className={styles.td}>{item.city}</td>
              <td className={styles.td}>{item.phoneNumber}</td>
              <td className={styles.td}>{item.details}</td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }

  return (
    <div>
      <UrgentTimerSy></UrgentTimerSy>
      <h1 className={styles.h1s}>
        الموقع يضع المعلومات في أيادي المبادرات فقط
      </h1>
      <h1 className={styles.warn}>وغير مسؤول عن الإستجابة للبلاغات</h1>
      <h2 className={styles.h2s}>
        هذه الصفحة للمساعدة في تجميع المعلومات حول الضحايا الأحياء العالقين تحت
        الركام{" "}
      </h2>
      <AddData data={data} setData={setData} />
      <input
        className={styles.input}
        type="text"
        placeholder="بحث"
        value={searchTerm}
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <DataTable data={showData} />
      <UrgentTimer className={styles.timer}></UrgentTimer>
    </div>
  );
}
