import React from "react";
import { FaHourglassHalf } from "react-icons/fa";
import styles from "./Spinner.module.css";

export default function Spinner() {
  return (
    <div className={styles.spinner}>
      <FaHourglassHalf className={styles.icon} />
    </div>
  );
}
