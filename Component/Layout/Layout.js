import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Nav, { SiteFooter } from "../Navbar/Nav";
import styles from "./Layout.module.css";

const Layout = ({ children }) => {
  const [top, setTop] = useState("13vh");
  const router = useRouter();
  useEffect(() => {
    if (router.pathname == "/") setTop("0vh !important");
    else setTop("15vh !important");
  }, [router.pathname]);

  return (
    <>
      <Nav />
      <div className={styles.container} style={{ marginTop: top }}>
        <main className={styles.main}>{children}</main>
      </div>
      <SiteFooter />
    </>
  );
};

export default Layout;
