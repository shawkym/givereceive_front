# Syria GIVE AND GET
Discord Server: https://discord.com/invite/Q8dCqHVkNz

Hi, 

To clone the project please use:
```bash
    git clone -b testing https://gitlab.com/antoin.sader/givereceive_front.git
```
Then make your own feature branch,
Please make sure to clone from testing and then commit to the pre-testing first.
if you feel sure about your modifications commit to testing. 
Tony Sader will see the changes and deploy it to the master.

PLEASE DO NOT CLONE, PUSH OR PULL TO/FROM THE MASTER.
If you have any question about this, please ask @Emil, @Shawky or @Tony

## SETUP:
First time:
on linux:
```bash
    npm install dotenv
    npm install 
```
on Windows:
```bash
    npm install -g win-node-env
    npm install
```
## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to run the frontend.


## Testing Deployment and Workflows

When you push your code to the `testing` or `pre-testing` branches it will be atuomatically deployed online to the following URLs respectively 
testing:
`https://givereceivetest.onrender.com` 
pre-testing:
`https://givereceive-beta.onrender.com` 

Also you can make a MR to the `pre-testing` branch that will cause render to deploy your feature branch on a test link:
 `https://givereceive-beta-pr-[pr-number].onrender.com`

that you will get as well inside the comments on your MR
    git clone -b testing https://gitlab.com/antoin.sader/givereceive_front.git

Please make sure to clone from testing branch and start your own feature branch then after you commit back to the testing if you feel sure about your changes.
or pre-testing otherwise.

Tony Sader will see the changes on testing and deploy it to the master.
PLEASE DO NOT CLONE, PUSH OR PULL TO/FROM THE MASTER.
If you have any question about this, please ask @Emil, @Shawky or @Tony


## Pages
PLEASE DO NOT ADD ANY .css, .module.css TO THE pages FOLDER. According to next.js, only page components are allowed in the pages folder.
PLEASE do not change the name of the files. THANKS!

The project is using Next JS and the following is a quick overview:

**index.js:** 
---------This is the main page of the website.
----------------
    <div  className="scroll-down-arrow-container">
    	<FaArrowDown  className="scroll-down-arrow"  />
    </div>
   can be removed if we don't want the arrow.
 
 -----------------

    <Header  />
   Is a component that contains the title and description of the website.
   
   ------------------
   

The background is set here:

    <div  className="homeRoot"  style={{ backgroundImage: "url('bg.webp')" }}>
-----

    <Suggestions  />
   Is a component that shows the quick search options which can be clicked and move to one of the pages give/receive
The suggestions are now in api.config.js



**give.js, receive:** 
---------Those pages are so simillar.
---
The comments in receive.js explaining the code.

-----

    <AddPanel  />
This component shows the "insertion" form with the search panel inside it.

----

    <Cards
    original={original}
    deleteItem={deleteItem}
    title="طلبات من محتاجين للمساعدة"
    showData={showData}
    setShowData={setShowData}
    center
    />

The cards component is showing the data in the form of cards. 
We are passing:
	

## If you have any problems
Please do not hesitate to contact the team on the team's discord server.
