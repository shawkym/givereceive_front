import { useState, useEffect } from "react";
import { NextSeo } from "next-seo";
import AddPanel from "../../Component/AddPanel/AddPanel";
import SearchPanel from "../../Component/SeacrchPanel/SearchPanel";
import CardPaginator from "../../Component/Paginator/cardPaginator";
import Spinner from "../../Component/Spinner/Spinner";
import NoticeHelper from "../../Component/Card/NoticeHelper";
import { useTranslation } from "react-i18next";

export default function CardsPage({ pageType }) {
  const [displayData, setDisplayData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [isFiltering, setIsFiltering] = useState(false);
  const [cardType, setCardType] = useState(pageType);
  const { t } = useTranslation();

  
  useEffect(() => {
  }, [displayData,isFiltering]);

  return (
    <div style={{ width: "100%" }}>
      {/* NoticeHelper component displays a notice on top of the page */}
      <NoticeHelper pageType={pageType.type} />
      <NextSeo title={t("SYRIAN_GIVE_AND_RECEIVE")} />
      <div className="cardPageRoot" style={{}}>
        {/* Add panel for adding new cards */}
        <div className="addPanelWrap" style={{}}>
          <AddPanel
            cardsType={pageType}
            displayedCards={displayData}
            setDisplayedCards={setDisplayData}
          />
        </div>
        <div className="searchWithCards">
          {/* Search panel for filtering and searching cards */}
          <SearchPanel
            displayedCards={displayData}
            setDisplayedCards={setDisplayData}
            cardsType={cardType}
            setIsFiltering={setIsFiltering}
          />
          {/* Card paginator for displaying cards, 10 cards is the api default*/}
          {
            <CardPaginator
              displayedCards={displayData}
              setDisplayedCards={setDisplayData}
              itemsPerPage={10}
              setIsLoading={setIsLoading}
              itemsType={cardType}
              isFiltering={isFiltering}
            />
          }
          {isLoading ? <Spinner /> : ""}
        </div>
      </div>
    </div>
  );
}

// get Page Type from URL
export async function getStaticPaths() {
  const paths = [
    { params: { cardsPage: "give" } },
    { params: { cardsPage: "receive" } },
    { params: { cardsPage: "lost" } },
    { params: { cardsPage: "found" } },
    { params: { cardsPage: "reclaim" } },
  ];
  return { paths, fallback: 'blocking' };
}

// pass page type to component
export async function getStaticProps({ params }) {
  const pageType = params.cardsPage;
  return {
    props: {
      pageType,
    },
  };
}
