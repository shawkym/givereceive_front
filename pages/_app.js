import Layout from "../Component/Layout/Layout";
import LanguageProvider from "../Contexts/LanguageContext";
import i18n from "../i18n";
import "../public/globals.css";
import styles from "../public/globals.css";

function MyApp({ Component, pageProps }) {
  return (
    <LanguageProvider>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </LanguageProvider>
  );
}

export default MyApp;
