import React from 'react';
import { Bilink } from "react-icons/bi";
function MyIframeComponent({ pagePath }) {
  const src = `/GDPR.pdf`;

  return (
    <iframe src={src} title={`Page: ${pagePath}`} width="100%" height="900px" />
  );
}

export default MyIframeComponent;
