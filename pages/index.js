//THIS IS THE MAIN PAGE
//CONSISTS OF HEADER WHICH IS THE TEXT ON THE FRONT AND THE SUGGESTIONS COMPONENT WHICH IS THE QUICK SEARCH
import { NextSeo } from "next-seo";

import { FaArrowDown } from "react-icons/fa";
import Head from 'next/head'

import Header from "../Component/Header/Header";
import HomeLanding from "../Component/HomeLanding/HomeLanding";

export default function Home() {
  return (
    <>
    <Head>
      <meta property="og:type" content="website" />
      <meta property="og:title" content="Maroc Donne & Reçois" />
      <meta property="og:description" content="هذا الموقع خاص للمساعدة في الربط بين المحليين الذين يستطيعون المساعدة والناس المحتاجة لها " />
      {<meta property="og:image" content="https://front-ma.onrender.com/bg.webp" />}
      {<meta property="og:url" content="https://front-ma.onrender.com" />}
        <NextSeo title="Maroc Donne & Reçois" description="Maroc Donne & Reçois" />
    </Head>

      <div className="homeMainDiv" style={{direction: "rtl"}}>
       
        <HomeLanding />

      </div>

    </>
  );
}
