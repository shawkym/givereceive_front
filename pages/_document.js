import { Html, Head, Main, NextScript } from 'next/document'
import { SiteFooter } from '../Component/Navbar/Nav'

export default function Document() {
  return (
    <Html>
      <Head />
      <body>
      {/* <div className="ribbon">06-02-2023</div> */}
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
