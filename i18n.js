import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import translationAR from "./public/locales/ar.json";
import translationTR from "./public/locales/tr.json";
import translationEN from "./public/locales/en.json";
import translationFR from "./public/locales/fr.json";
import LanguageDetector from "i18next-browser-languagedetector";

const resources = {
  en: {
    translation: translationEN,
  },
  ar: {
    translation: translationAR,
  },
  // tr: {
  //   translation: translationTR,
  // },
  fr: {
    translation: translationFR,
  },
};

i18n
  .use(LanguageDetector) // add language detector
  .use(initReactI18next)
  .init({
    detection: {
      // enable language detection
      order: ["querystring", "localStorage", "sessionStorage", "navigator", "htmlTag", "path", "subdomain"],
      lookupLocalStorage: 'i18nextLng',
      lookupSessionStorage: 'i18nextLng',
      caches: ['localStorage'],
      excludeCacheFor: ['cimode']
    },
    resources: resources,
    fallbackLng: "ar",
  });

export default i18n;
